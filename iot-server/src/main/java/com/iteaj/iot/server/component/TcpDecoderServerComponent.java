package com.iteaj.iot.server.component;

import com.iteaj.iot.codec.filter.CombinedInterceptor;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.server.ServerMessage;
import com.iteaj.iot.server.TcpServerComponent;

/**
 * create time: 2021/2/20
 *
 * @author iteaj
 * @since 1.0
 */
public abstract class TcpDecoderServerComponent<M extends ServerMessage> extends TcpServerComponent<M> {

    public TcpDecoderServerComponent(ConnectProperties connectProperties) {
        super(connectProperties);
    }

    public TcpDecoderServerComponent(ConnectProperties connectProperties, CombinedInterceptor filter) {
        super(connectProperties, filter);
    }

    @Override
    public abstract String getName();

}
