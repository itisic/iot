package com.iteaj.iot.tools.db;

public interface IotJdbc {

    /**
     * jdbc数据库操作模板
     * @param entity 操作的实体 方便根据此实体获取不同的数据源
     * @return
     */
    void jdbcTemplate(Object entity);
}
