package com.iteaj.iot.websocket;

import com.iteaj.iot.message.DefaultMessageHead;

public class WebSocketCloseHead extends DefaultMessageHead {

    public WebSocketCloseHead(String equipCode) {
        super(equipCode, null, WebSocketProtocolType.Close);
    }
}
