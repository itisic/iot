package com.iteaj.iot.plc;

import com.iteaj.iot.utils.DataType;

public class ReadAddress {

    /**
     * 读取的长度(字节)
     */
    private short length;

    /**
     * 地址
     */
    private String address;

    /**
     * 数据类型
     */
    private DataType dataType;

    /**
     * 是否按位读取
     */
    private AddressType type;

    protected ReadAddress(String address, AddressType type) {
        this(address, (short) 0, type);
    }

    protected ReadAddress(String address, short length, AddressType type) {
        this.type = type;
        this.length = length;
        this.address = address;
    }

    public ReadAddress(String address, DataType dataType) {
        this.address = address;
        this.dataType = dataType;
    }

    /**
     * @param address M100, I100, Q100, DB1.100
     * @return
     */
    public static ReadAddress buildBoolRead(String address) {
        return new ReadAddress(address, (short) 1, AddressType.Bit);
    }

    /**
     * 以字节为读取单位
     * @param address M100, I100, Q100, DB1.100
     * @param length 读取的字节数
     * @return
     */
    public static ReadAddress buildByteRead(String address, short length) {
        return new ReadAddress(address, length, AddressType.Word);
    }

    /**
     * 读取short值
     * @param address
     * @return
     */
    public static ReadAddress buildShortRead(String address) {
        return new ReadAddress(address, DataType.Short);
    }

    /**
     * 读取UShort值
     * @param address
     * @return
     */
    public static ReadAddress buildUShortRead(String address) {
        return new ReadAddress(address, DataType.UShort);
    }

    /**
     * 读取int值
     * @param address
     * @return
     */
    public static ReadAddress buildIntRead(String address) {
        return new ReadAddress(address, DataType.Int);
    }

    /**
     * 读取UInt值
     * @param address
     * @return
     */
    public static ReadAddress buildUIntRead(String address) {
        return new ReadAddress(address, DataType.UInt);
    }

    /**
     * 读取Float值
     * @param address
     * @return
     */
    public static ReadAddress buildFloatRead(String address) {
        return new ReadAddress(address, DataType.Float);
    }

    /**
     * 读取Double值
     * @param address
     * @return
     */
    public static ReadAddress buildDoubleRead(String address) {
        return new ReadAddress(address, DataType.Double);
    }

    /**
     * 读取String值
     * @param address
     * @param length 字节长度
     * @return
     */
    public static ReadAddress buildStringRead(String address, int length) {
        return new ReadAddress(address, DataType.String);
    }

    /**
     * 读取Boolean值
     * @param address
     * @return
     */
    public static ReadAddress buildBooleanRead(String address) {
        return new ReadAddress(address, DataType.Boolean);
    }

    public short getLength() {
        return length;
    }

    public ReadAddress setLength(short length) {
        this.length = length;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public ReadAddress setAddress(String address) {
        this.address = address;
        return this;
    }

    public AddressType getType() {
        return type;
    }
}
