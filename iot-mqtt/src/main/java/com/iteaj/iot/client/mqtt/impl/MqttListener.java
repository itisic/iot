package com.iteaj.iot.client.mqtt.impl;

/**
 * mqtt监听器
 */
public interface MqttListener {

    /**
     * 订阅监听
     */
    void onSubscribe(DefaultMqttSubscribeProtocol protocol);
}
