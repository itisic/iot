package com.iteaj.iot.modbus;

import com.iteaj.iot.Protocol;
import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.format.DataFormat;
import com.iteaj.iot.format.DataFormatConvert;
import com.iteaj.iot.modbus.consts.ModbusCode;

/**
 * 居于iot框架实现的通用的Modbus操作协议
 */
public interface ModbusCommonProtocol extends Protocol {

    /**
     * 获取设备响应负载
     * @return
     */
    Payload getPayload();

    /**
     * 获取设备响应负载
     * @param format 指定的格式
     * @return
     */
    default Payload getPayload(DataFormat format) {
        if(this.getPayload() instanceof ReadPayload) {
            ((ReadPayload) this.getPayload()).setConvert(DataFormatConvert.getInstance(format));
        }

        return this.getPayload();
    }

    /**
     * @see ModbusCode or other
     * @return
     */
    @Override
    ProtocolType protocolType();
}
