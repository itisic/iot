package com.iteaj.iot.test;

public interface TestConst {

    String LOGGER_PROTOCOL_DESC = "组件测试({}) 类型：{} - 客户端编号：{} - messageId: {} - 状态：{}";

    String LOGGER_MQTT_PROTOCOL_DESC = "解码器测试({}) 类型：{} - topic：{} - 客户端编号：{} - messageId: {} - 状态：{}";

    String LOGGER_API_DESC = "接口测试({}) 接口：{} - 状态：{}";

    String LOGGER_SERIAL_DESC = "串口测试 测试类型: {} - 状态：{}";

    String LOGGER_PROTOCOL_FUNC_DESC = "协议接口测试({}) 接口：{} - 客户端编号：{} - 状态：{}";

    String LOGGER_DATA_COLLECT_DESC = "数据采集器测试({}) - 类型：{} - 状态：{}";

    String LOGGER_MODBUS_DESC = "Modbus测试({}) 类型：{} - 客户端编号：{} - messageId: {} - 寄存器：{} - 状态：{}({})";
}
