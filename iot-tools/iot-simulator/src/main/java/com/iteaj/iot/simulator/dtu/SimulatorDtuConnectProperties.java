package com.iteaj.iot.simulator.dtu;

import com.iteaj.iot.ProtocolHandle;
import com.iteaj.iot.simulator.SimulatorClientMessage;
import com.iteaj.iot.simulator.SimulatorConnectProperties;
import com.iteaj.iot.simulator.SimulatorProtocolType;

/**
 * dtu连接配置信息
 */
public class SimulatorDtuConnectProperties extends SimulatorConnectProperties {

    /**
     * 心跳间隔(秒)
     */
    private int interval;

    /**
     * 是否发送心跳包
     */
    private boolean heartbeat;

    /**
     * 是否发送注册包
     */
    private boolean register;

    /**
     * 心跳包
     */
    private String heartbeatMsg;

    /**
     * 设备编号(注册包)
     */
    private String deviceSn;

    /**
     * 协议处理器
     */
    private ProtocolHandle<SimulatorDtuProtocol> protocolHandle;

    /**
     * 默认不指定任何业务处理器
     */
    public SimulatorDtuConnectProperties(String remoteHost, Integer remotePort, String deviceSn) {
        this(remoteHost, remotePort, deviceSn, null);
    }

    /**
     * @param remoteHost
     * @param remotePort
     * @param deviceSn 当前dtu的设备编号, 也是注册包
     * @param handle 此客户端的业务处理器
     */
    public SimulatorDtuConnectProperties(String remoteHost, Integer remotePort, String deviceSn, ProtocolHandle<SimulatorDtuProtocol> handle) {
        super(remoteHost, remotePort, SimulatorProtocolType.DTU, deviceSn);
        this.register = true;
        this.deviceSn = deviceSn;
        this.protocolHandle = handle;
    }

    public boolean isHeartbeat() {
        return heartbeat;
    }

    public SimulatorDtuConnectProperties setHeartbeat(boolean heartbeat) {
        this.heartbeat = heartbeat; return this;
    }

    public int getInterval() {
        return interval;
    }

    public SimulatorDtuConnectProperties setInterval(int interval) {
        this.interval = interval; return this;
    }

    public String getHeartbeatMsg() {
        return heartbeatMsg;
    }

    public SimulatorDtuConnectProperties setHeartbeatMsg(String heartbeatMsg) {
        this.heartbeatMsg = heartbeatMsg; return this;
    }

    public boolean isRegister() {
        return register;
    }

    public SimulatorDtuConnectProperties setRegister(boolean register) {
        this.register = register; return this;
    }

    public String getDeviceSn() {
        return deviceSn;
    }

    public void setDeviceSn(String deviceSn) {
        this.deviceSn = deviceSn;
    }

    public ProtocolHandle getProtocolHandle() {
        return protocolHandle;
    }

    public SimulatorDtuConnectProperties setProtocolHandle(ProtocolHandle<SimulatorDtuProtocol> protocolHandle) {
        this.protocolHandle = protocolHandle; return this;
    }

    @Override
    public SimulatorDtuProtocol getProtocol(SimulatorClientMessage message) {
        return new SimulatorDtuProtocol(message);
    }
}
